module redis-load-test

go 1.16

require (
	github.com/go-redis/redis/v9 v9.0.0-rc.1
	go.k6.io/k6 v0.34.1
)
