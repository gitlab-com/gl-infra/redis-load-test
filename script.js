import { Trend, Counter } from 'k6/metrics';
import redis from 'k6/x/redis';
import redis_cluster from 'k6/x/redis_cluster';
import redis_sentinel from 'k6/x/redis_sentinel';
import { randomString, randomIntBetween } from 'https://jslib.k6.io/k6-utils/1.1.0/index.js';
import { SharedArray }  from 'k6/data';

const traces = JSON.parse(open(__ENV.TRACE_PATH || './trace.json'));

// Setup redis client
let client;

if (__ENV.REDIS_CLUSTER_NODE) {
  // We only need to start with *one* cluster node; for tests, we can be sure that this exists
  // even if, in real life, we'd supply two or more (all?) to be sure we can connect
  // to one of them successfully to obtain the full cluster info
  client = new redis_cluster.Client({
    addrs: [__ENV.REDIS_CLUSTER_NODE],
    password: __ENV.REDIS_PASSWORD || '',
    db: Number.parseInt(__ENV.REDIS_DB || 0),
    pool_size: 1,
  });
} else if (__ENV.REDIS_SENTINELS) {
  let sentinel_options = {
    master_name: __ENV.REDIS_MASTER_NAME || 'gitlab-redis',
    sentinel_addrs: __ENV.REDIS_SENTINELS.split(','),
    password: __ENV.REDIS_PASSWORD || '',
    db: Number.parseInt(__ENV.REDIS_DB || 0),
    pool_size: 1,
  };

  if (__ENV.REDIS_SENTINEL_PASSWORD !== '') {
    sentinel_options.sentinel_password = __ENV.REDIS_SENTINEL_PASSWORD;
  }

  client = new redis_sentinel.Client(sentinel_options);
} else {
  client = new redis.Client({
    addr: __ENV.REDIS_URL || 'localhost:6379',
    password: __ENV.REDIS_PASSWORD || '',
    db: Number.parseInt(__ENV.REDIS_DB || 0),
    pool_size: 1,
  });
}

// Similar to randomString, but don't care about randomness just length
// and generate in chunks (not adding a char at a time) for better speed
function arbitraryString(length) {
  const charset = 'abcdefghijklmnopqrstuvwxyz0123456789';
  let res = '';
  while (length > charset.length) {
    res += charset
    length -= charset.length
  }
  res += charset.substr(0,length);
  return res;
}

function scenarioID(cmd, pattern) {
  return `${cmd}-${pattern}`.replace(/[^a-zA-Z0-9_\-]/gi, '_');
}

function keyID(cmd, pattern, bucket) {
  return `${scenarioID(cmd, pattern)}-${bucket}`;
}

// Create a certain amount of random keys following the pattern
function generateKeys(pattern, frequency) {
  if (frequency <= 0) {
    frequency = 1;
  }
  return [...Array(frequency).keys()].map(function() {
    return pattern.replace(/(\$PATTERN|\$UUIDISH|\$LONGHASH|\$HASH)/gi, randomString(64))
                  .replace(/\$NUMBER/gi, randomIntBetween(1, 1000000));
  })
}

// Classic weighted random algorithm. The prepare step is for optimization.
// This generates an accumulated sum of occurrences. The search step later uses
// binary search to pick the range
function prepareWeightedRandom(distribution) {
  let weights = [];
  let weight = 0;
  for (const [_, count] of Object.entries(distribution)) {
    weight += count
    weights.push(weight);
  }
  return [weight, Object.keys(distribution), weights]
}

function weightedRandom(total, values, weights) {
  const weight = randomIntBetween(0, total);
  let start = 0;
  let end = values.length - 1;
  while (start < end) {
    let mid = Math.floor((start + end) / 2);
    if (weights[mid] >= weight) {
      if (mid == 0) {
         end = 0
      } else {
         end = mid - 1;
      }
    } else {
      start = mid + 1
    }
  }
  return values[end];
}

// Find which key patterns are numeric only, so that when we populate their data
// we put numbers in, not strings
let numericOnlyKeys = []
for (const [cmd, keys] of Object.entries(traces)) {
  if (cmd.startsWith("incr") || cmd.startsWith("decr")) {
    for (const pattern of Object.keys(keys)) {
      numericOnlyKeys.push(pattern);
    }
  }
}

function populateData(cmd, keys, distribution, pattern) {
  let [total, values, weights] = prepareWeightedRandom(distribution);
  let numeric = numericOnlyKeys.includes(pattern);
  keys.forEach(function(key) {
    const size = weightedRandom(total, values, weights);
    if (size != 0) {
      eval(`prepare_${cmd}`)(key, size, numeric)
    }
  })
}

let randomKeys = {}
let randomSizeTotal = {}
let randomSizeWeights = {}
let randomSizeValues = {}
for (const [cmd, keys] of Object.entries(traces)) {
  for (const [pattern, keyData] of Object.entries(keys)) {
    const scenario = scenarioID(cmd, pattern);
    let [total, values, weights] = prepareWeightedRandom(keyData.value_size);
    randomSizeTotal[scenario] = total
    randomSizeValues[scenario] = values
    randomSizeWeights[scenario] = weights

    for (let i of Array(10).keys()) {
      // Share key spaces between pub/sub operations
      let k = keyID(cmd, pattern, i);
      if (cmd == 'unsubscribe' || cmd == 'publish') {
        k = keyID('subscribe', pattern, i)
      }
      randomKeys[k] = new SharedArray(k, function () {
        // SharedArray is executed once and only once. The data is shared between VUS
		let freq = keyData.unique_key_freq_percentiles[i];
		if (freq <= 0) {
			freq = 1
		}
        let keys = generateKeys(pattern, freq);
        if (__ENV.SKIP_DATA_POPULATION) {
          console.log(`- Skip data population for ${freq} keys of ${k}`)
        } else {
          console.log(`- Preparing data for ${freq} keys of ${k}`)

          try {
            populateData(cmd, keys, keyData.response_size, pattern)
          } catch(e) {
            console.log(e)
          }
        }
        return keys
      });
    }
  }
}

let scenarios = {};

for (const [cmd, keys] of Object.entries(traces)) {
  for (const [pattern, keyData] of Object.entries(keys)) {
    const multiplier = Number.parseInt(__ENV.MULTIPLIER || 1);
    const keyRate = Object.values(keyData.value_size).reduce((sum, x) => sum + x) * multiplier;

    scenarios[scenarioID(cmd, pattern)] = {
      exec: cmd,
      executor: 'constant-arrival-rate',
      rate: keyRate,
      timeUnit: __ENV.TRACE_DURATION || '30s', // The sample taken rate. We are taking tcpdump in 30 seconds.
      duration: __ENV.DURATION || '1m',
      preAllocatedVUs: __ENV.VUS || 1,
      env: {
        CMD: cmd,
        PATTERN: pattern,
      }
    }
  }
}

if(__ENV.WORKHORSE_SUBSCRIBERS > 0) {
  // A special VU that listens to workhorse notifications
  scenarios["workhorse-notifications-subscription"] = {
    exec: "workhorseSubscribe",
    executor: 'constant-vus',
    vus: __ENV.WORKHORSE_SUBSCRIBERS|| 300,
    duration: __ENV.DURATION || '1m',
  }
}

// The workhorse subscribe needs to know the duration in seconds; convert this once
const durationUnits = __ENV.DURATION.substr(-1);
var multiplier = 1;

switch(durationUnits) {
  case 'm':
    multiplier = 60;
    break;
  case 'h':
    multiplier = 60 * 60;
    break;
  default:
    //either explicitly seconds "s", or can't tell in which case we assume seconds
    multiplier = 1;
}
const duration = parseInt(__ENV.DURATION) * multiplier;

export let options = {
  scenarios: scenarios
}

// Setup client-side metrics
let RedisLatencyMetrics = {}
let RedisCounterMetrics = {}
client.keys().forEach(function(command) {
  RedisLatencyMetrics[command] = new Trend(`redis_latency_${command}`, true);
  RedisCounterMetrics[command] = new Counter(`redis_counter_${command}`);
});


// Wrapper to include observability stuff
let observe = function(proc) {
  const start = Date.now();
  // Ignore the actual result, it doesn't matter
  // Errors will be thrown as exceptions
  try {
    proc();
  } catch (e) {
    console.log(e);
  }
  const latency = Date.now() - start;

  RedisLatencyMetrics[__ENV.CMD].add(latency);
  RedisCounterMetrics[__ENV.CMD].add(1);
}

// Get a random pre-setup key
// - Pick a random bucket (0 - 9)
// - Pick a random key from the unique keys inside a bucket
let getKey = function(cmd, pattern) {
  if (cmd == 'unsubscribe' || cmd == 'publish') {
    // Share the key space between pub/sub operations
    cmd = 'subscribe'
  }

  let bucket = randomIntBetween(0, 9);
  let key = keyID(cmd, pattern, bucket);
  let length = randomKeys[key].length;

  return randomKeys[key][randomIntBetween(0, length - 1)];
}

let getValueSize = function(cmd, pattern) {
  let scenario = scenarioID(cmd, pattern);
  return weightedRandom(
    randomSizeTotal[scenario],
    randomSizeValues[scenario],
    randomSizeWeights[scenario]
  )
}

export function workhorseSubscribe() {
  // Long-lived subscription (length of test) to match production-like workloads
  // No need to observe, because we 'wait' inside the call, so that we don't have far too many of these running
  client.subscribe("workhorse:notifications", duration*1000, true);
}

// Redis execution methods
// - The key is retrieved via __ENV.PATTERN
// - A random request size for the test is retreive via getValueSize()
// - A random response size for the test is retreive via getResponseSize()
export function setex() {
  let value = numericOnlyKeys.includes(__ENV.PATTERN) ? randomIntBetween(1, 100) : arbitraryString(getValueSize(__ENV.CMD, __ENV.PATTERN));
  observe(() => client.setex(getKey(__ENV.CMD, __ENV.PATTERN), value, 15))
}
export function prepare_setex(_key, _size) { /* Do nothing */}

export function sadd() {
  observe(() => client.sadd(getKey(__ENV.CMD, __ENV.PATTERN), arbitraryString(getValueSize(__ENV.CMD, __ENV.PATTERN))));
}
export function prepare_sadd(_key, _size) { /* Do nothing */}

export function get() {
  observe(() => client.get(getKey(__ENV.CMD, __ENV.PATTERN)));
}
export function prepare_get(key, size, numericOnly) {
  client.set(key, (numericOnly ? randomIntBetween(1, 100) : arbitraryString(size)))
}

export function set() {
  let value = numericOnlyKeys.includes(__ENV.PATTERN) ? randomIntBetween(1, 100) : arbitraryString(getValueSize(__ENV.CMD, __ENV.PATTERN));;
  observe(() => client.set(getKey(__ENV.CMD, __ENV.PATTERN), value, 0));
}
export function prepare_set(_key, _size) { /* Do nothing */}

export function pfadd() {
  observe(() => client.pfadd(getKey(__ENV.CMD, __ENV.PATTERN), 'myelement'));
}
export function prepare_pfadd(_key, _size) { /* Do nothing */}

export function expire() {
  // Expiry time will need some thought; may be relative to pattern
  // and the other flags may matter too but for now assume none.
  observe(() => client.expire(getKey(__ENV.CMD, __ENV.PATTERN), 15));
  // Can't do this; there's too many possibilities (not just numeric, but also sets)
  // Return 1 key because the key will disappear
  //let value = numericOnlyKeys.includes(__ENV.PATTERN) ? randomIntBetween(1, 100) : arbitraryString(getValueSize(__ENV.CMD, __ENV.PATTERN));
  //client.set(getKey(__ENV.CMD, __ENV.PATTERN), value, 0)
}
export function prepare_expire(_key, _size) { /* After some iterations, the key is removed */}

export function pexpire() {
  // As for expire ^^
  observe(() => client.pexpire(getKey(__ENV.CMD, __ENV.PATTERN), 15000));
  // Can't do this; there's too many possibilities (not just numeric, but also sets)
  // Return 1 key because the key will disappear
  //client.set(getKey(__ENV.CMD, __ENV.PATTERN), arbitraryString(10), 0)
}
export function prepare_pexpire(_key, _size) { /* After some iterations, the key is removed */}

export function hget() {
  observe(() => client.hget(getKey(__ENV.CMD, __ENV.PATTERN), 'myfield'));
}
export function prepare_hget(key, size) {
  client.hset(key, 'myfield', arbitraryString(size))
}

export function hset() {
  observe(() => client.hset(getKey(__ENV.CMD, __ENV.PATTERN), 'myfield', arbitraryString(getValueSize(__ENV.CMD, __ENV.PATTERN))));
}
export function prepare_hset(_key, _size) { /* Do nothing */}

export function hmset() {
  observe(() => client.hmset(getKey(__ENV.CMD, __ENV.PATTERN), 'myfield', randomString(getValueSize(__ENV.CMD, __ENV.PATTERN))));
}
export function prepare_hmset(_key, _size) { /* Do nothing */}

export function hdel() {
  observe(() => client.hdel(getKey(__ENV.CMD, __ENV.PATTERN), 'myfield'));
}
export function prepare_hdel(key, _size) {
  // Delete a field of a non-existent key is different from delete a non-existent field
  client.hset(key, 'myfield2', 'myvalue')
}

export function exists() {
  observe(() => client.exists(getKey(__ENV.CMD, __ENV.PATTERN)));
}
export function prepare_exists(key, _size) {
  client.set(key, arbitraryString(64))
}

export function incrbyfloat() {
  observe(() => client.incrbyfloat(getKey(__ENV.CMD, __ENV.PATTERN), Math.random() * 1000));
}
export function prepare_incrbyfloat(_key, _size) { /* Do nothing */ }

export function incr() {
  observe(() => client.incr(getKey(__ENV.CMD, __ENV.PATTERN)));
}
export function prepare_incr(_key, _size) { /* Do nothing */ }

export function publish() {
  observe(() => client.publish(getKey(__ENV.CMD, __ENV.PATTERN), arbitraryString(getValueSize(__ENV.CMD, __ENV.PATTERN))));
}
export function prepare_publish(_key, _size) { /* Do nothing */}

export function del() {
  observe(() => client.del(getKey(__ENV.CMD, __ENV.PATTERN)));
}
export function prepare_del(_key, _size) { /* Do nothing */}

export function zadd() {
  // Doesn't handle the flags; not sure we use them either.
  // Need to do some variation on 'score' (second arg)
  observe(() => client.zadd(getKey(__ENV.CMD, __ENV.PATTERN), Math.random() * 1000, arbitraryString(getValueSize(__ENV.CMD, __ENV.PATTERN))));
}
export function prepare_zadd(_key, _size) { /* Do nothing */}

export function zcard() {
  observe(() => client.zcard(getKey(__ENV.CMD, __ENV.PATTERN)));
}
export function prepare_zcard(key, size) {
  for (let i = 0; i < 10 * (size - 1); i++) {
    client.zadd(key, i, arbitraryString(10))
  }
}

export function incrby() {
  observe(() => client.incrby(getKey(__ENV.CMD, __ENV.PATTERN), randomIntBetween(2,20)));
}
export function prepare_incrby(_key, _size) { /* Do nothing */ }

export function zremrangebyrank() {
  observe(() => client.zremrangebyrank(getKey(__ENV.CMD, __ENV.PATTERN), randomIntBetween(1, 100), randomIntBetween(101,200)));
}
export function prepare_zremrangebyrank(key, _size) {
  for (let i = 0; i < 10; i++) {
    client.zadd(key, i, arbitraryString(10))
  }
}

export function decr() {
  observe(() => client.decr(getKey(__ENV.CMD, __ENV.PATTERN)));
}
export function prepare_decr(_key, _size) { /* Do nothing */ }

export function smembers() {
  observe(() => client.smembers(getKey(__ENV.CMD, __ENV.PATTERN)));
}
export function prepare_smembers(key, size) {
  // This is tough. Add (size / 10) members, 10 bytes each.
  for (let i = 0; i < size / 10; i++) {
    client.sadd(key, arbitraryString(10))
  }
}

export function mget() {
  observe(() => client.mget([getKey(__ENV.CMD, __ENV.PATTERN)]));
}
export function prepare_mget(key, size) {
  client.set(key, arbitraryString(size))
}

export function srem() {
  observe(() => client.srem(getKey(__ENV.CMD, __ENV.PATTERN), 'myvalue'));
}
export function prepare_srem(key, _size) {
  for (let i = 0; i < 10; i++) {
    client.sadd(key, arbitraryString(10))
  }
}

export function subscribe() {
  // Average is 0.5s give or take a bit
  // Do *not* wait inside the subscribe; return, let the goroutine wait and then unsub,
  // and continue to dispatch new subscribe's at the desired rate.
  observe(() => client.subscribe(getKey(__ENV.CMD, __ENV.PATTERN), randomIntBetween(10, 1000), false));
}
export function prepare_subscribe(_key, _size) { /* Do nothing */}

export function unsubscribe() {
  // We handle unsubscribes directly within subscribe (see redis.go)
  //observe(() => client.set(getKey(__ENV.CMD, __ENV.PATTERN), 'myvalue', 0));
}
export function prepare_unsubscribe(_key, _size) { /* Do nothing */}

export function zrevrange() {
  observe(() => client.zrevrange(getKey(__ENV.CMD, __ENV.PATTERN), 'myvalue', 0));
}
export function prepare_zrevrange(key, size) {
  for (let i = 0; i < 10 * (size - 1); i++) {
    client.zadd(key, i, arbitraryString(10))
  }
}

export function scard() {
  observe(() => client.scard(getKey(__ENV.CMD, __ENV.PATTERN)));
}
export function prepare_scard(key, size) {
  for (let i = 0; i < 10 * (size - 1); i++) {
    client.sadd(key, arbitraryString(10))
  }
}

export function zrangebyscore() {
  observe(() => client.zrangebyscore(getKey(__ENV.CMD, __ENV.PATTERN), "1", "2"));
}
export function prepare_zrangebyscore(key, size) {
  for (let i = 0; i < 10 * (size - 1); i++) {
    client.zadd(key, i, arbitraryString(10))
  }
}

export function zremrangebyscore() {
  observe(() => client.zremrangebyscore(getKey(__ENV.CMD, __ENV.PATTERN), "1", "2"));
}
export function prepare_zremrangebyscore(key, size) {
  for (let i = 0; i < 10 * (size - 1); i++) {
    client.zadd(key, i, arbitraryString(10))
  }
}

export function blpop() {
  observe(() => client.blpop(5, getKey(__ENV.CMD, __ENV.PATTERN)));
}
export function prepare_blpop(_key, _size) { /* Do nothing */}

export function lpush() {
  observe(() => client.lpush(getKey(__ENV.CMD, __ENV.PATTERN), arbitraryString(getValueSize(__ENV.CMD, __ENV.PATTERN))));
}
export function prepare_lpush(_key, _size) { /* Do nothing */}

export function sismember() {
  observe(() => client.sismember(getKey(__ENV.CMD, __ENV.PATTERN), arbitraryString(getValueSize(__ENV.CMD, __ENV.PATTERN))));
}

export function prepare_sismember(key, size) {
  for (let i = 0; i < 10 * (size - 1); i++) {
    client.sadd(key, arbitraryString(10))
  }
}

export function hmget() {
  observe(() => client.hmget([getKey(__ENV.CMD, __ENV.PATTERN)], ['myfield']));
}
export function prepare_hmget(key, size) {
  client.hset(key, 'myfield', arbitraryString(size))
}

export function ttl() {
  observe(() => client.ttl(getKey(__ENV.CMD, __ENV.PATTERN)));
}
export function prepare_ttl(_key, _size) { /* Do nothing */ }

export function unlink() {
  observe(() => client.unlink(getKey(__ENV.CMD, __ENV.PATTERN)));
}
export function prepare_unlink(_key, _size) { /* Do nothing */}

export function sscan() {
  // TODO: this is complicated, but current usage is negligible, so ignore it entirely for now
}
export function prepare_sscan(_key, _size) { /* Do nothing */}
